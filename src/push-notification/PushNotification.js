import React from 'react';
import axios from 'axios';
import { Button, Card, Icon, Table, Header, Message } from 'semantic-ui-react'
import { urlB64ToUint8Array } from '../utils/cryptoUtil';

export default class PushNotification extends React.Component {
  constructor(props) {
    super(props);
    this.subscribeNotification = this.subscribeNotification.bind(this);
    this.vapidPublicKey = 'BA-FqKzHtS67LDetAZ6K3zRy5Dv6jI2d6zlsjBFQU0555rL_7nPG36AB0neoat-zO3ilqFtXTYkQlT2ha4oQxUo';
    this.state = { subscribed: false };
  }

  subscribeNotification(event) {
    console.log('subscribing for push notification');

    navigator.serviceWorker.ready.then(registration => {
      if (!registration.pushManager) {
        alert("Push Unsupported");
        return
      }
      console.log(`vapid public key${this.vapidPublicKey}`);
      registration.pushManager
        .subscribe({
          userVisibleOnly: true,
          applicationServerKey: urlB64ToUint8Array(this.vapidPublicKey),
        })
        .then(subscription => axios.post("/subscribe", subscription))
        .then(res => this.setState({subscribed: true}))
        .catch(err => console.error("Push subscription error: ", err))
    });
  }

  render() {
    return (
      <div style={{marginTop: '1rem'}}>
        <Card style={{width: '90%', margin: '0 auto'}}>
          <Card.Content>
            <Card.Header>
              <Icon  color='teal' name='plane' size='large' />
              Voxxed Airlines
            </Card.Header>
            <br/>
            <Card.Meta>
              <strong>
                <h3>Booking Number 5HQER</h3>
              </strong>
            </Card.Meta>
            <Card.Description>
              <Table celled padded>
                <Table.Body>
                  <Table.Row>
                      <Table.Cell><Header as='h4'>Flight Number</Header></Table.Cell>
                      <Table.Cell>VX121</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><Header as='h4'>From</Header></Table.Cell>
                    <Table.Cell>HongKong</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><Header as='h4'>To</Header></Table.Cell>
                    <Table.Cell>Singapore</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><Header as='h4'>Departure</Header></Table.Cell>
                    <Table.Cell>9:00 AM</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell><Header as='h4'>Arrival</Header></Table.Cell>
                    <Table.Cell>1:00 PM</Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            {this.state.subscribed
              ?
              <Message
                icon='alarm outline'
                header='Thank you for subscribing to our notification.'
                content=''
              />
              : (
              <div className='ui two buttons'>
                <Button color='teal' onClick={this.subscribeNotification}>Notify Me for flight delays</Button>
              </div>)
            }
          </Card.Content>
        </Card>
      </div>
    );
  }
}
