import React from 'react';
import { Button } from 'semantic-ui-react';
import Presentation from './presentation';
import PushNotification from './push-notification/PushNotification';
import Payments from './payments/Payments';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.setPage = this.setPage.bind(this);
    this.state = {page: ''}
  }

  setPage(e) {
    this.setState({page: e.target.name});
  }

  render() {
    if(this.state.page === '') {
      return (
        <Button.Group vertical>
          <Button name="slides" onClick={this.setPage}>Slides</Button>
          <Button name="push" onClick={this.setPage}>Push Notification</Button>
          <Button name="payments" onClick={this.setPage}>Payments</Button>
        </Button.Group>
      );
    }

    if(this.state.page === 'push') {
      return (
        <div>
         <PushNotification />
        </div>
      );
    }

    if(this.state.page === 'slides') {
      return (
        <div>
          <Presentation />
        </div>
      );
    }

    if(this.state.page === 'payments') {
      return (
        <div>
          <Payments />
        </div>
      );
    }

  }
}