/*eslint-disable no-undef */
import React, {Component} from 'react';
import { Button, Card, Icon, Table, Header, Message } from 'semantic-ui-react'

export default class Payments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      supported: true,
    };
    this.pay = this.pay.bind(this);
  }

  pay() {
    if(!window.PaymentRequest) {
      this.setState({ supported: false });
      return;
    }

    const applePayMethod = {
      supportedMethods: "https://apple.com/apple-pay",
      data: {
        version: 3,
        merchantIdentifier: "merchant.com.example",
        merchantCapabilities: ["supports3DS", "supportsCredit", "supportsDebit"],
        supportedNetworks: ["amex", "discover", "masterCard", "visa"],
        countryCode: "US",
      },
    };

    const otherPaymentMethod = {
      supportedMethods: ['basic-card'],
      data: {
        supportedNetworks: [
          'amex', 'discover', 'masterCard', 'visa'
        ],
        countryCode: 'US',
        validationEndpoint: '/applepay/validate/',
        merchantIdentifier: 'merchant.com.agektmr.payment'
      }
    };

    // Supported payment methods
    var supportedInstruments = [otherPaymentMethod, applePayMethod];

    // Checkout details
    var details = {
      displayItems: [{
        label: 'Original ticket price',
        amount: { currency: 'SGD', value: '400.00' }
      }, {
        label: 'Super early bird discount',
        amount: { currency: 'SGD', value: '200.00' }
      }],
      total: {
        label: 'Total due',
        amount: { currency: 'SGD', value : '200.00' }
      }
    };

    let paymentRequest = new PaymentRequest(supportedInstruments, details);
    paymentRequest.show().then((result) => {
      this.setState({status: 'success'});
      result.complete('success');
    });
  }

  render() {
    const applePayStyle = {};
    applePayStyle['-webkit-appearance'] = '-apple-pay-button';
    if (this.state.supported) {
      return (
        <div style={{width: '90%', margin: '0 auto', marginTop:'1rem'}}>
          <Card fluid>
            <Card.Content>
              <Card.Header>
                Order Summary
              </Card.Header>
              <Card.Meta>
                <strong>VOXXED DAYS SINGAPORE 2019</strong>
              </Card.Meta>
              <Card.Description>
                <Table celled padded>
                  <Table.Body>
                    <Table.Row>
                      <Table.Cell><Header as='h4'>Ticket Type</Header></Table.Cell>
                      <Table.Cell>One Day Pass</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Header as='h4'>Date & Venue</Header></Table.Cell>
                      <Table.Cell>01-June-2019, Marina Bay Sands</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Header as='h4'>Ticket Price</Header></Table.Cell>
                      <Table.Cell>SGD 400</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Header as='h4'>Early Bird Discount</Header></Table.Cell>
                      <Table.Cell>50%</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                      <Table.Cell><Header as='h4'>Payment Amount</Header></Table.Cell>
                      <Table.Cell>SGD 200</Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </Card.Description>
            </Card.Content>
            <Card.Content extra>
              {
                (this.state.status === 'success')
                ? <Message
                    icon='checkmark'
                    header='Thank you for purchasing ticket. See you next year @Voxxed Days Singapore.'
                    content=''
                  />
                : (
                    <div className='ui two buttons'>
                      <Button basic color='green' onClick={this.pay}>Pay</Button>
                      <Button basic color='red'>Cancel</Button>
                    </div>
                  )
              }

            </Card.Content>
          </Card>

    {/* {
              (window.ApplePaySession && ApplePaySession.canMakePayments())?
                <button style={applePayStyle} className="btnPayment" onClick={this.pay} />
                :
                <button style={applePayStyle} className="btnPayment" onClick={this.pay}>
                  Buy Voxxed Days Ticket (2019)
                </button>
            } */}

        </div>
      );
    }

    if(!(this.state.supported)) {
      return (
        <div>
          <h3>Your browser version does not support Payment API </h3>
        </div>
      );
    }

  }

}
