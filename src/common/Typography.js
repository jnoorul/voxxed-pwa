import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Text } from 'spectacle';

export function Title(props) {
  return (<Heading textSize="10vw" style={props.style}>{props.children}</Heading>);
}

export function BodyText(props) {
  return (<Text textSize="6vw" style={props.style}>{props.children}</Text>);
}

export function EmptyLine(props) {
  let emptyRows = [];
  for(let i = 0; i < props.rows; i += 1) {
    emptyRows.push(<BodyText>&nbsp;</BodyText>);
  }
  return emptyRows;
}

EmptyLine.props = {
  rows: PropTypes.number,
};

EmptyLine.defaultProps = {
  rows: 1
};