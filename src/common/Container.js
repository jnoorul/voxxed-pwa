import React from 'react';
import { Slide } from 'spectacle';
import styled from 'styled-components';

const SlideContainer = styled.div`
  height: 100vh;
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  justify-content: space-between;
`;

const PWASlide = function (props) {
  return (
    <Slide {...props} maxHeight="100vh" style={{height: '100vh'}}>
      <SlideContainer>
        {props.children}
      </SlideContainer>
    </Slide>
  );
};

const SlideHeader = function(props) {
  const style = {
    flex: '1 1',
    display: 'flex',
    flexFlow: 'column wrap',
    justifyContent: 'center',
  };
  return (
    <div style={style}>
      <div style={{flex: 0}}>{props.children}</div>
    </div>
  );
};

const SlideBody = function(props) {
  const style = {
    flex: '3 3',
    display: 'flex',
    flexFlow: 'column wrap',
    justifyContent: 'center',
  };
  return (
    <div style={style}>
      <div style={{flex: 0, height: '90%'}}>{props.children}</div>
    </div>
  );
};

const SlideFooter = styled.div`
  flex: 0.5 0.5;
`;

export { PWASlide, SlideHeader, SlideBody, SlideFooter}