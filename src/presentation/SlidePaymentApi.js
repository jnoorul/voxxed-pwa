/*eslint-disable no-undef */
import React, {Component} from 'react';
import {Slide} from 'spectacle';
import {Title, BodyText, EmptyLine} from '../common/Typography';
import { PWASlide, SlideHeader, SlideBody, SlideFooter } from '../common/Container';

export default class SlidePaymentApi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      supported: true,
    };
    this.pay = this.pay.bind(this);
  }

  pay() {
    if(!window.PaymentRequest) {
      this.setState({ supported: false });
      return;
    }

    const applePayMethod = {
      supportedMethods: "https://apple.com/apple-pay",
      data: {
        version: 3,
        merchantIdentifier: "merchant.com.example",
        merchantCapabilities: ["supports3DS", "supportsCredit", "supportsDebit"],
        supportedNetworks: ["amex", "discover", "masterCard", "visa"],
        countryCode: "US",
      },
    };

    const otherPaymentMethod = {
      supportedMethods: ['basic-card'],
      data: {
        supportedNetworks: [
          'amex', 'discover', 'masterCard', 'visa'
        ],
        countryCode: 'US',
        validationEndpoint: '/applepay/validate/',
        merchantIdentifier: 'merchant.com.agektmr.payment'
      }
    };

    // Supported payment methods
    var supportedInstruments = [otherPaymentMethod, applePayMethod];

    // Checkout details
    var details = {
      displayItems: [{
        label: 'Original ticket price',
        amount: { currency: 'SGD', value: '289.00' }
      }, {
        label: 'Super early bird discount',
        amount: { currency: 'SGD', value: '89.00' }
      }],
      total: {
        label: 'Total due',
        amount: { currency: 'SGD', value : '200.00' }
      }
    };

    let paymentRequest = new PaymentRequest(supportedInstruments, details);
    paymentRequest.show().then((result) => {
      this.setState({status: 'success'});
      result.complete('success');
    });
  }

  render() {
    const applePayStyle = {};
    applePayStyle['-webkit-appearance'] = '-apple-pay-button';
    if (this.state.supported && this.state.status === '') {
      return (
        <PWASlide align="center flex-start">
          <SlideHeader>
            <Title>Payment Integration</Title>
          </SlideHeader>
          <SlideBody>
            <BodyText>Enjoyed the conference so far?</BodyText>
            <BodyText>Want to come again?</BodyText>
            <EmptyLine />
            {
              (window.ApplePaySession && ApplePaySession.canMakePayments())?
                <button style={applePayStyle} className="btnPayment" onClick={this.pay} />
                :
                <button style={applePayStyle} className="btnPayment" onClick={this.pay}>
                  Buy Voxxed Days Ticket (2019)
                </button>
            }
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
      );
    }

    if(!(this.state.supported)) {
      return (
        <PWASlide>
          <BodyText>Your browser version does not support Payment API </BodyText>
        </PWASlide>
      );
    }

    if(this.state.status === 'success') {
      return (
        <PWASlide>
          <BodyText>Ticket Purchased successfully</BodyText>
        </PWASlide>
      );
    }
  }

}
