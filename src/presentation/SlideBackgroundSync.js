/*eslint-disable no-useless-constructor  */
import React, {Component} from 'react';
import { Slide } from 'spectacle';
import { Title, BodyText, EmptyLine } from '../common/Typography';
import { PWASlide, SlideHeader, SlideBody, SlideFooter } from '../common/Container';

export default class SlideBackgroundSync extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <PWASlide>
        <SlideHeader>
          <Title>Background Sync</Title>
        </SlideHeader>
        <SlideBody>
          <div style={{flex: 0}}>
            <BodyText>Send Notification when I am online</BodyText>
            <EmptyLine rows={1} />
            <button className="btnPrimary">Notify via background sync</button>
          </div>
        </SlideBody>
        <SlideFooter>&nbsp;</SlideFooter>
      </PWASlide>
    );
  }
}
