import React,{ Component } from 'react';
import axios from 'axios';
import { Slide } from 'spectacle';
import { BodyText,Title } from '../common/Typography';
import { urlB64ToUint8Array } from '../utils/cryptoUtil';

export default class SlidePushNotification extends Component {
  constructor(props) {
    super(props);
    this.sendPushMessage = this.sendPushMessage.bind(this);
    this.vapidPublicKey = 'BA-FqKzHtS67LDetAZ6K3zRy5Dv6jI2d6zlsjBFQU0555rL_7nPG36AB0neoat-zO3ilqFtXTYkQlT2ha4oQxUo';
  }

  sendPushMessage(event) {
    if(!event.target.checked) {
      console.log('push notification disabled');
      return;
    }
    console.log('push notification enabled');
    console.log(`notification.permission- ${Notification.permission}`);

    navigator.serviceWorker.ready.then(registration => {
      if (!registration.pushManager) {
        alert("Push Unsupported");
        return
      }

      console.log(`vapid public key${this.vapidPublicKey}`);
      registration.pushManager
        .subscribe({
          userVisibleOnly: true,
          applicationServerKey: urlB64ToUint8Array(this.vapidPublicKey),
        })
        .then(subscription => axios.post("/subscribe", subscription))
        .catch(err => console.error("Push subscription error: ", err))
    });
  }
  render(){
    return (
      <Slide>
        <Title>Enable Push Notification</Title>
        <BodyText>&nbsp;</BodyText>
        <label className="switch">
          <input onChange={this.sendPushMessage} type="checkbox" />
          <span className="slider round" />
        </label>
      </Slide>
    );
  }
}
