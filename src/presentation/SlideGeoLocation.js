import React from 'react';
import {Slide, Table, TableRow, TableItem, TableBody} from 'spectacle';
import { Title, BodyText, EmptyLine } from '../common/Typography';
import { PWASlide, SlideHeader, SlideBody, SlideFooter } from '../common/Container';
import { round } from '../utils/mathUtil';


export default class SlideGeoLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state =  {
      supported: true,
      permissioned: true,
      status: 'none',
      locationData: {}
    };
    this.fetchLocationData = this.fetchLocationData.bind(this);
  }

  fetchLocationData() {
    if(!navigator.geolocation) {
      this.setState({supported: false});
      return;
    }
    navigator.geolocation.getCurrentPosition(position => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
      this.getAddress(latitude, longitude).then((res) => {
        this.setState({status: 'success', supported: true,
          locationData: {
            latitude: round(latitude, 2),
            longitude: round(longitude, 2),
            address: res.results[0].formatted_address,
          }});
      });
    });
  }

  getAddress(latitude, longitude) {
    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyAEL9HdKNmRUuo5YfOKs3PTucarxSSe4S0`;
    return new Promise((resolve, reject) => {
        fetch(url)
          .then(res => res.json())
          .then(jsonRes => resolve(jsonRes))
          .catch(err => reject(err));
      }
    );
  }

  render() {
    if(!this.state.supported) {
      return (
        <PWASlide>
          <SlideHeader>
            <Title>Geo Location</Title>
          </SlideHeader>
          <SlideBody>
            <BodyText>Location service is not supported in your browser version</BodyText>
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
    );
    }

    if(this.state.status === 'none') {
      return (
        <PWASlide>
          <SlideHeader>
            <Title>Geo Location</Title>
          </SlideHeader>
          <SlideBody>
            <BodyText>Where am I?</BodyText>
            <BodyText><button className="btnPrimary" onClick={this.fetchLocationData}>Locate me!</button></BodyText>
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
      );
    }


    if(this.state.supported && !this.state.permissioned) {
      return (
        <PWASlide>
          <SlideHeader>
            <Title>Geo Location</Title>
          </SlideHeader>
          <SlideBody>
            <BodyText>Please grant me permission to use Location Service.</BodyText>
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
      );
    }

    if(this.state.status === 'loading') {
      return (
        <PWASlide>
          <SlideHeader>
            <Title>Geo Location</Title>
          </SlideHeader>
          <SlideBody>
            <BodyText>Identifying your location ....</BodyText>
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
      );
    }

    if(this.state.status === 'success') {
      return (
        <PWASlide>
          <SlideHeader>
            <Title>Geo Location</Title>
          </SlideHeader>
          <SlideBody>
            <Table>
              <TableBody>
                <TableRow>
                  <TableItem>Latitude</TableItem>
                  <TableItem>{this.state.locationData.latitude}</TableItem>
                </TableRow>
                <TableRow>
                  <TableItem>Longitude</TableItem>
                  <TableItem>{this.state.locationData.longitude}</TableItem>
                </TableRow>
                <TableRow>
                  <TableItem>Address</TableItem>
                  <TableItem>{this.state.locationData.address}</TableItem>
                </TableRow>
              </TableBody>
            </Table>
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
      );
    }

    if(this.state.status === 'error') {
      return (
        <PWASlide>
          <SlideHeader>
            <Title>Geo Location</Title>
          </SlideHeader>
          <SlideBody>
            <BodyText>OOPS! Location service did not work.</BodyText>
          </SlideBody>
          <SlideFooter>&nbsp;</SlideFooter>
        </PWASlide>
      );
    }
  }
}

//https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
//res.results[0].formatted_address
//AIzaSyAEL9HdKNmRUuo5YfOKs3PTucarxSSe4S0