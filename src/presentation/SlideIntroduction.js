/*eslint-disable no-useless-constructor  */
import React, {Component} from 'react';
import { PWASlide, SlideBody, SlideFooter } from '../common/Container';
import { Title, BodyText, EmptyLine } from '../common/Typography';

export default class SlideIntroduction extends Component{
  constructor(props){
    super(props);
    this.promptA2HS = this.promptA2HS.bind(this);
    this.state = { disableA2HS: false };
  }

  promptA2HS() {
    let promptEvent = this.props.a2hs.promptEvent;
    if(promptEvent) {
      this.setState({ disableA2HS: true });
      promptEvent.prompt();
      // Wait for the user to respond to the prompt
      promptEvent.userChoice
        .then((choiceResult) => {
          if (choiceResult.outcome === 'accepted') {
            console.log('User accepted the A2HS prompt');
          } else {
            console.log('User dismissed the A2HS prompt');
          }
        });
    }
  }

  render() {
    return (
      <PWASlide>
        <SlideBody>
          <Title>Progressive Web Application</Title>
          <EmptyLine rows={2}/>
          <BodyText>NoorulAmeen</BodyText>
          <BodyText>Ritesh Mehrotra</BodyText>
          {
            (this.props.a2hs.visible)
              ? <button disabled={this.state.disableA2HS} onClick={this.promptA2HS} className="btnA2HS">Install Voxxed-PWA</button>
              : null
          }
        </SlideBody>
        <SlideFooter>&nbsp;</SlideFooter>
      </PWASlide>
    );
  }
}