// import {
//   Appear, BlockQuote, Cite, CodePane, Code, Deck, Fill, Fit,
//   Image, Layout, ListItem, List, Quote, Slide, Heading, Text
// } from 'spectacle';
import React from 'react';
import { Title } from '../common/Typography';
import { Slide, Deck } from 'spectacle';
import SlidePushNotification from './SlidePushNotification';
import SlideIntroduction from './SlideIntroduction';
import SlidePaymentApi from './SlidePaymentApi';
import SlideGeoLocation from './SlideGeoLocation';
import SlideBackgroundSync from './SlideBackgroundSync';

export default class Presentation extends React.Component {
  constructor(props) {
    super(props);
    this.showInstallButton = this.showInstallButton.bind(this);
    this.state = { a2hs: { visible: false, promptEvent: null } };
  }

  showInstallButton(e) {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    this.setState({a2hs: { visible: true, promptEvent: e }});
  }

  componentDidMount() {
    window.addEventListener('beforeinstallprompt', this.showInstallButton);
  }

  render() {
    return (
      <Deck>
        <SlideIntroduction a2hs={this.state.a2hs}/>
        <SlideBackgroundSync/>
        <SlidePushNotification/>
        <SlidePaymentApi/>
        <SlideGeoLocation/>
        <Slide>
          <Title>Why do we need PWA?</Title>
        </Slide>
        <Slide>
          <Title>What makes a Good PWA?</Title>
        </Slide>
        <Slide>
          <Title>Building Blocks</Title>
        </Slide>
        <Slide>
          <Title>Let's build a sample app</Title>
        </Slide>
        <Slide>
          <Title>How does PWA matches to Native Apps</Title>
        </Slide>
        <Slide>
          <Title>Success Stories</Title>
        </Slide>
        <Slide>
          <Title>Thank you</Title>
        </Slide>
      </Deck>
    );
  }
}