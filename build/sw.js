importScripts("/precache-manifest.c0eccd5c53c9a8ab2c3fbbc2d697d1dd.js", "https://storage.googleapis.com/workbox-cdn/releases/3.1.0/workbox-sw.js");

/*eslint-disable no-restricted-globals */
/*eslint-disable no-undef */

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

workbox.precaching.precacheAndRoute(self.__precacheManifest || []);
workbox.skipWaiting();
workbox.clientsClaim();

self.addEventListener("push", event => {
  console.log('notification received'+event.data.text());
  const pushMessage = event.data.text();

  const body = {
    title: 'Voxxed Airlines',
    icon: '/techtalks-icon-192.png',
    body: pushMessage
  };

  event.waitUntil(self.registration.showNotification('Voxxed Airlines', body));
});
